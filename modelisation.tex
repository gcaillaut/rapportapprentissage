\section{Modélisation}

Dans cette section, nous allons voir comment modéliser des individus, dans le but d'en apprendre leur caractéristiques.

\subsection{Généralisation}

L'objectif d'un algorithme d'apprentissage est de savoir \emph{reconnaitre} deux individus d'une même classe,
et, à l'inverse, de savoir dissocier deux individus de deux classes différentes.
Pour ce faire, la première étape de l'apprentissage consiste à dégager les propriétés communes au sein d'une même classe.

Pour reprendre l'exemple du tri de paysage, un paysage marin possèdera à la fois du sable et la mer tandis
qu'un paysage rural sera composé de champs et de verdure.

\begin{figure}[H]
  \centering
  \begin{minipage}[c]{0.5\textwidth}
    \centering
    \includegraphics[width=0.8\linewidth]{resources/paysage_marin.jpg}
    \caption{Un paysage marin}
    \label{fig:paysagemarin}
  \end{minipage}%
  \begin{minipage}[c]{0.5\textwidth}
    \centering
    \includegraphics[width=0.8\linewidth]{resources/paysage_rural.jpg}
    \caption{Un paysage rural}
    \label{fig:paysagerural}
  \end{minipage}
\end{figure}

Les propriétés communes pourront alors s'exprimer sous la forme :
\begin{verbatim}
avoirSable(X)
avoirMer(X)
avoirChamp(X)
avoirVerdure(X)
\end{verbatim}
Avec X l'image courante.
\newline

Les propriétés ainsi trouvées pourront ensuite être liées aux images, et plusieurs propriétés pourront être rattachés
à une image.
Ainsi, une image rurale sera représenté de la sorte :
\begin{figure}[H]
  \center
  \[avoirVerdure(ImageRural) \land avoirChamp(ImageRural)\]
  \caption{Représentation logique d'un paysage rural}
  \label{fig:reprLogiquePaysageRural}
\end{figure}
Autrement dit, les individus seront représentés par une conjonction de prédicats, et chaque prédicat représentera une
propriété.


\subsection{Apprentissage}

L'apprentissage utilise deux espaces distincts : l'espace des  \emph{exemples} et l'espace des \emph{hypothèses}.
Le premier représente l'ensemble des individus réels, ceux qui serviront pour apprendre une classification.
Le second représentera l'ensemble des concepts pouvant être attribué à un élément de l'ensemble des exemples.
L'espace des hypothèse est en fait un ensemble de predicats sur l'ensemble des exemples.

\subsubsection{L'espace des hypothèses}

Soit $I$ l'ensemble des exemples et $H$ l'ensemble des hypothèses, $\forall i \in I, h \in H$ alors $h(i)$ est la
valeur prédite par $h$ pout $i$. $h$ peut être vue comme une conjonction de prédicats.

Les hypothèses de $H$ sont ordonnées selon une relation de généralité $\geq$. Ainsi, on a
\[ \top \land \top \geq avoirVerdure(i) \land \top \]
De cette façon, on peut connaitre l'hypothèse la plus générale $\top \land \ldots \land \top$ et l'hypothèse
la plus spécifique $\bot \land \ldots \land \bot$.

L'odre de l'espace des hypothèses permet d'organiser la recherche au sein de cet espace. Si une hypothèse $h$ rejète
une instance $i$, il est alors inutile de chercher dans les hypothèses plus spécifique que $h$.


\subsubsection{Maximum généralisé}

Le maximum généralisé est l'hypothèse qui généralise un ensemble d'hypothèses, tout en étant la plus
spécifique possible.

C'est une notion importante, car c'est a partir du maximum généralisé que l'on saura dire si un exemple
fait parti, ou non, d'un concept.

\subsubsection{Version space}

\emph{Version space} est un ensemble de concepts consistants, un ensemble d'exemples positifs et un
ensemble d'exemples dégatifs.

Une instance $i$ est consistante si $h(x) = c(x)$ avec c(x) vrai si $x \in ExemplesPositifs$.

A partir de cet espace, il est possible d'apprendre des concepts. Par exemple, grâce à l'algorithme
{\bfseries CANDIDATE-ELIMINATION}. Cet algorithme va faire converger l'hypothèse la plus spécifique et l'hypothèse
la plus générale vers le maximum généralisé du concept voulu.

Par exemple, ici, nous allons apprendre le concept \emph{voiture japonaise économique}\footnote{exemple tiré de \url{http://cse-wiki.unl.edu/wiki/index.php/Concept_Learning_and_the_General-to-Specific_Ordering}}.

\begin{figure}[H]
  \center
  \includegraphics[width=1.0\linewidth]{resources/table_jap_cars.jpg}
  \caption{Ensemble d'apprentissage}
  \label{fig:japCars}
\end{figure}

\paragraph{Initialisation}

Soit $S$ l'ensemble des bornes inférieures (donc spécifiques) et $G$ l'ensemble des bornes supérieures (donc générales).

On initialise $S$ tel qu'il contienne l'hypothèse la plus spécifique : $S = \{(\bot, \bot, \bot, \bot, \bot)\}$
On initialise $G$ tel qu'il conitenne l'hypothèse la plus générale : $G = \{(\top, \top, \top, \top, \top)\}$.

L'algorithme consiste à parcourir l'ensemble des exemples et, selon s'il est positif ou négatif,
généraliser $S$ ou restreindre $G$.

\paragraph{Étape 1}
$S = \{(\bot, \bot, \bot, \bot, \bot)\}$ et $G = \{(\top, \top, \top, \top, \top)\}$.

$x1 = (Japan, Honda, Blue, 1980, Economy)$ et $x$ est un exemple positif.
On va alors généraliser $S$ : $S = \{(Japan, Honda, Blue, 1980, Economy)\}$.

\paragraph{Étape 2}
$S = \{(Japan, Honda, Blue, 1980, Economy)\}$ et $G = \{(\top, \top, \top, \top, \top)\}$.

$x2 = (Japan, Toyota, Green, 1970, Sports)$ et $x2$ est un exemple négatif.
On va alors restreindre $G$ : \[G = \{(USA, \top, \top, \top, \top), (\top, Honda, \top, \top, \top), (\top, \top, Blue, \top, \top), (\top, \top, \top, 1980, \top), (\top, \top, \top, \top, Economy)\}\]

La borne supérieure se restreind et n'acceptera que les exemples de la marque Honda, ou de couleur bleue, ou datant de 1980 ou encore de type économique. Autrement dit, $x2$ sera rejetté.


\paragraph{Étape 3}
\[G = \{(USA, \top, \top, \top, \top), (\top, Honda, \top, \top, \top), (\top, \top, Blue, \top, \top), (\top, \top, \top, 1980, \top), (\top, \top, \top, \top, Economy)\}\] et $S = \{(Japan, Honda, Blue, 1980, Economy)\}$.
$x3 =  (Japan, Toyota, Blue, 1990, Economy)$ et $x3$ est un exemple positif.

On va modifier $G$ de sorte à ce qu'il accepte $x3$, mais que $x2$ soit toujours refusé :
$G = \{(\top, \top, Blue, \top, \top), (\top, \top, \top, \top, Economy)\}$.

Et, puisque $x3$ est un exemple positif, on va généraliser $S$ afin d'inclure $x3$.
$S = \{(Japan, \top, Blue, \top, Economy)\}$.

\paragraph{Étape 4}
$G = \{(\top, \top, Blue, \top, \top), (\top, \top, \top, \top, Economy)\}$
et $S = \{(Japan, \top, Blue, \top, Economy)\}$.

$x4 = (USA, Chrysler, Red, 1980, Economy)$ et $x4$ est un exemple négatif.
On va donc spécialiser $G$ afin d'exclure $x4$, tout en prenant garde à rester cohérent avec $S$.

$G = \{(\top, \top, Blue, \top, \top), (Japan, \top, \top, \top, Economy)\}$.
Le premier élément n'est pas modifié car $x4$ est rejetté, c'est donc le second qui a été impacté.

\paragraph{Étape 5}
$G = \{(\top, \top, Blue, \top, \top), (Japan, \top, \top, \top, Economy)\}$ et
$S = \{(Japan, \top, Blue, \top, Economy)\}$.

$x5 = (Japan, Honda, White, 1980, Economy)$ et $x5$ est un exemple positif.
On va donc modifier $G$ pour qu'il accepte $x5$ : $G = \{(Japan, \top, \top, \top, Economy)\}$.
Puis, on va généraliser $S$ afin qu'il accepte aussi $x5$ : $S = \{(Japan, \top, \top, \top, Economy)\}$

\paragraph{Résultat}
L'algorithme se termine lorsque $G$ et $S$ ont convergé en un même singleton.
Ici, le résultat est $(Japan, \top, \top, \top, Economy)$. C'est le maximum généralisé des voitures japonaises économique.
L'ordinateur a donc appris à reconnaitre ce concept, il acceptera les exemples plus spécifiques
que $(Japan, \top, \top, \top, Economy)$, et rejettera les autres.


